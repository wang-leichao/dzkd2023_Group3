package com.dzkd.soft.mapper;

import com.dzkd.soft.pojo.GuaranteeBLM;
import org.apache.ibatis.annotations.Param;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

public interface GuaranteeBLMMapper {
    List<GuaranteeBLMMapper> selectByPage(@Param("index") int index,
                                    @Param("limit") int limit,
                                    @Param("bLName") String bLName,
                                    @Param("bLRuleNumb") String bLRuleNumb,
                                    @Param("riskNumb") String riskNumb,
                                    @Param("affectServerNumb") String affectServerNumb,
                                    @Param("bLClassification") String bLClassification,
                                    @Param("state") String state,
                                    @Param("lastExecuteTime")Timestamp lastExecuteTime,
                                    @Param("operation") String operation
                                    );

    int selectCount(@Param("bLName") String bLName,
                    @Param("bLRuleNumb") String bLRuleNumb,
                    @Param("riskNumb") String riskNumb,
                    @Param("affectServerNumb") String affectServerNumb,
                    @Param("bLClassification") String bLClassification,
                    @Param("state") String state,
                    @Param("lastExecuteTime") Timestamp lastExecuteTime,
                    @Param("operation") String operation);

    void insert(@Param("guaranteeBLM") GuaranteeBLM guaranteeBLM);
    void deleteById(Integer id);
    void update(@Param("id") Integer id,
                @Param("riskNumb") String riskNumb,
                @Param("affectServerNumb") String affectServerNumb,
                @Param("state") String state,
                @Param("lastExecuteTime")Timestamp lastExecuteTime,
                @Param("operation") String operation);
}
