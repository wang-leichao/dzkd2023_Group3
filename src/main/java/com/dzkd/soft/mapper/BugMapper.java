package com.dzkd.soft.mapper;

import com.dzkd.soft.pojo.Bug;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BugMapper {   // 数据库的接口调用通道

    Bug selectById(Integer id);

    Bug selectByIdAndbugname(@Param("id") Integer id,
                              @Param("bugname") String bugname);

    Bug selectByIdAndbugnameAndbuglevel(@Param("id") Integer id,
                                         @Param("bugname") String bugname,
                                         @Param("buglevel") String buglevel);

    Bug selectBybug(@Param("bug") Bug bug);

    List<Bug> selectBybuglevel(@Param("buglevel") String buglevel);

    Bug login(@Param("bug") Bug bug);

    List<Bug> selectByPage(@Param("index") int index,
                           @Param("limit") int limit,
                           @Param("bugname") String bugname,
                           @Param("buglevel") String buglevel);

    int selectCount(@Param("bugname") String bugname,
                    @Param("buglevel") String buglevel);

    void insert(@Param("bug") Bug bug);

    void deleteById(Integer id);

    void updatenum(@Param("id") Integer id, @Param("num") Integer num);
}
