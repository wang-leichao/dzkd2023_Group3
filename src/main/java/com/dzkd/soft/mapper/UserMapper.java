package com.dzkd.soft.mapper;

import com.dzkd.soft.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {   // 数据库的接口调用通道

    User selectById(Integer id);

    User selectByIdAndUsername(@Param("id") Integer id,
                               @Param("username") String username);

    User selectByIdAndUsernameAndPassword(@Param("id") Integer id,
                                          @Param("username") String username,
                                          @Param("password") String password);

    User selectByUser(@Param("user") User user);

    List<User> selectByPassword(@Param("password") String password);

    User login(@Param("user") User user);

    List<User> selectByPage(@Param("index") int index,
                            @Param("limit") int limit,
                            @Param("username") String username,
                            @Param("age") String age);

    List<User> selectByPage_Black(@Param("index") int index,
                                  @Param("limit") int limit,
                                  @Param("username") String username,
                                  @Param("age") String age);

    int selectCount(@Param("username") String username,
                    @Param("age") String age);

    int selectCount_Black(@Param("username") String username,
                    @Param("age") String age);

    void insert(@Param("user") User user);

    void insert_Black(@Param("user") User user);

    void updateLastLoginTime(@Param("user") User user);

    void deleteById(Integer id);

    void deleteById_Black(Integer id);

    void updateAge(@Param("id") Integer id, @Param("age") Integer age);
}
