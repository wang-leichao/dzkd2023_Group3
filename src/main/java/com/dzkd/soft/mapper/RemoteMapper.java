package com.dzkd.soft.mapper;

import com.dzkd.soft.pojo.Remoter;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RemoteMapper {
    int selectCount(@Param("deal_status") String deal_status,
                    @Param("warn_type") String warn_type,
                    @Param("host_ip") String host_ip);

    List<Remoter> selectByPage(@Param("index") int index,
                               @Param("limit") int limit,
                               @Param("deal_status") String deal_status,
                               @Param("warn_type") String warn_type,
                               @Param("host_ip") String host_ip);

    void updateDealStatus(@Param("id") Integer id, @Param("deal_status") String deal_status);


}
