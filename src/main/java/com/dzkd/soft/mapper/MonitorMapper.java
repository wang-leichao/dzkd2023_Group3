package com.dzkd.soft.mapper;

import com.dzkd.soft.pojo.Monitor;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MonitorMapper {   // 数据库的接口调用通道

    Monitor selectById(Integer id);

    Monitor selectByIdAndPortnum(@Param("id") Integer id,
                                  @Param("portnum") String portnum);

    Monitor selectByIdAndPortnumAndPortagreement(@Param("id") Integer id,
                                             @Param("portnum") String portnum,
                                             @Param("portagreement") String portagreement);

    Monitor selectByMonitor(@Param("monitor") Monitor monitor);

    List<Monitor> selectByPortagreement(@Param("portagreement") String portagreement);

    Monitor login(@Param("monitor") Monitor monitor);

    List<Monitor> selectByPage(@Param("index") int index,
                               @Param("limit") int limit,
                               @Param("portnum") String portnum,
                               @Param("processname") String processname);

    int selectCount(@Param("portnum") String portnum,
                    @Param("processname") String processname);

    void insert(@Param("monitor") Monitor monitor);

    void deleteById(Integer id);
}
