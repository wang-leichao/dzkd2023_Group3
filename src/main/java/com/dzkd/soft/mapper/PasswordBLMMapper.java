package com.dzkd.soft.mapper;

import com.dzkd.soft.pojo.PasswordBLM;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PasswordBLMMapper {

    PasswordBLM selectById(Integer id);

    int selectCount(@Param("name") String name,
                    @Param("category") String category);

    List<PasswordBLM> selectByPage(@Param("index") int index,
                            @Param("limit") int limit,
                            @Param("name") String name,
                            @Param("category") String category,
                            @Param("description") String description,
                            @Param("level") String level);

    void insert(@Param("passwordBLM") PasswordBLM passwordBLM);

    void deleteById(Integer id);

    void updateLevel(@Param("id") Integer id,
                   @Param("level") String level);

}
