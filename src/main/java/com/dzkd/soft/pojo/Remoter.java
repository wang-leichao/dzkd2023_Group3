package com.dzkd.soft.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.Printer;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;

public class Remoter {

    private Integer id;
    private String danger_level;
    private String warn_type;
    private String process_cmd;
    private String host_ip;
    private String deal_status;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp recent_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDanger_level() {
        return danger_level;
    }

    public void setDanger_level(String danger_level) {
        this.danger_level = danger_level;
    }

    public String getWarn_type() {
        return warn_type;
    }

    public void setWarn_type(String warn_type) {
        this.warn_type = warn_type;
    }

    public String getProcess_cmd() {
        return process_cmd;
    }

    public void setProcess_cmd(String process_cmd) {
        this.process_cmd = process_cmd;
    }

    public String getHost_ip() {
        return host_ip;
    }

    public void setHost_ip(String host_ip) {
        this.host_ip = host_ip;
    }

    public String getDeal_status() {
        return deal_status;
    }

    public void setDeal_status(String deal_status) {
        this.deal_status = deal_status;
    }

    public Timestamp getRecent_time() {
        return recent_time;
    }

    public void setRecent_time(Timestamp recent_time) {
        this.recent_time = recent_time;
    }
}
