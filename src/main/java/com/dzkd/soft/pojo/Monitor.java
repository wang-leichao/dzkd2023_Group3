package com.dzkd.soft.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;

public class Monitor {   // 数据库表结构的映射对象
    private Integer id;   // 每一个java属性对应数据库表中的每一个字段名
    private String portnum;
    private String portagreement;
    private Integer processname;
    private Integer hostnum;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPortnum() {
        return portnum;
    }

    public void setPortnum(String portnum) {
        this.portnum = portnum;
    }

    public String getPortagreement() {
        return portagreement;
    }

    public void setPortagreement(String portagreement) {
        this.portagreement = portagreement;
    }

    public Integer getProcessname() {
        return processname;
    }

    public void setProcessname(Integer processname) {
        this.processname = processname;
    }

    public Integer getHostnum() {
        return hostnum;
    }

    public void setHostnum(Integer hostnum) {
        this.hostnum = hostnum;
    }
    // 接收字符串日期，转换为date对象，前端必须传递 "2020-06-20 01:01:01"  这样的格式

}
