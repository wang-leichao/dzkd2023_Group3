package com.dzkd.soft.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;

public class GuaranteeBLM {
    private Integer id;
    private String bLName;
    private Integer bLRuleNumb;
    private Integer riskNumb;
    private Integer affectServerNumb;
    private String bLClassification;
    private String state;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp lastExecuteTime;
    private String operation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getbLName() {
        return bLName;
    }

    public void setbLName(String bLName) {
        this.bLName = bLName;
    }

    public Integer getbLRuleNumb() {
        return bLRuleNumb;
    }

    public void setbLRuleNumb(Integer bLRuleNumb) {
        this.bLRuleNumb = bLRuleNumb;
    }

    public Integer getRiskNumb() {
        return riskNumb;
    }

    public void setRiskNumb(Integer riskNumb) {
        this.riskNumb = riskNumb;
    }

    public Integer getAffectServerNumb() {
        return affectServerNumb;
    }

    public void setAffectServerNumb(Integer affectServerNumb) {
        this.affectServerNumb = affectServerNumb;
    }

    public String getbLClassification() {
        return bLClassification;
    }

    public void setbLClassification(String bLClassification) {
        this.bLClassification = bLClassification;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Timestamp getLastExecuteTime() {
        return lastExecuteTime;
    }

    public void setLastExecuteTime(Timestamp lastExecuteTime) {
        this.lastExecuteTime = lastExecuteTime;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
}
