package com.dzkd.soft.service;

import com.dzkd.soft.mapper.RemoteMapper;
import com.dzkd.soft.pojo.Remoter;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RemoteService {
    @Resource
    private RemoteMapper remoteMapper;

    public Map<String, Object> selectByPage(int page, int limit, String deal_status, String warn_type,String host_ip) {
        // page 页码   (page-1)*limit=起始下标index
        int index = (page - 1) * limit;   // 计算出起始下标
        List<Remoter> remoterList = remoteMapper.selectByPage(index, limit, deal_status, warn_type,host_ip);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "查询成功");
        map.put("count", remoteMapper.selectCount(deal_status, warn_type,host_ip));
        map.put("data", remoterList);
        return map;
    }
    public Map<String, Object> updateDealStatus(Integer id, String deal_status) {
        remoteMapper.updateDealStatus(id, deal_status);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "处理完成");
        return map;
    }


}
