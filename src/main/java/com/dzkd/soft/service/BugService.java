package com.dzkd.soft.service;

import com.dzkd.soft.mapper.BugMapper;
import com.dzkd.soft.pojo.Bug;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BugService {

    @Resource
    private BugMapper bugMapper;

    public Bug selectById(Integer id) {   // 业务逻辑层
        return bugMapper.selectById(id);
    }

    public Bug selectByIdAndBugname(Integer id, String bugname) {
        return bugMapper.selectByIdAndbugname(id, bugname);
    }

    public Bug selectByBug(Bug bug) {
        return bugMapper.selectBybug(bug);
    }

    public List<Bug> selectBybuglevel(String buglevel) {
        return bugMapper.selectBybuglevel(buglevel);
    }


    public Map<String, Object> selectByPage(int page, int limit, String bugname, String buglevel) {
        // page 页码   (page-1)*limit=起始下标index
        int index = (page - 1) * limit;   // 计算出起始下标
        List<Bug> bugList = bugMapper.selectByPage(index, limit, bugname, buglevel);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "查询成功");
        map.put("count", bugMapper.selectCount(bugname, buglevel));
        map.put("data", bugList);
        return map;
    }

    public Map<String, Object> insert(Bug bug) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis()); // 写入一个注册时间 new Date()代表当前服务器系统时间
        bug.setRegtime(timestamp);


        bugMapper.insert(bug);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "新增漏洞成功");
        return map;
    }

    public Map<String, Object> deleteById(Integer id) {
        bugMapper.deleteById(id);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "删除漏洞成功");
        return map;
    }

    public Map<String, Object> updatenum(Integer id, Integer num) {
        bugMapper.updatenum(id, num);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "更新漏洞成功");
        return map;
    }
}
