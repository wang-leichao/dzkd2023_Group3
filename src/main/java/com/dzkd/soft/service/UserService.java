package com.dzkd.soft.service;

import com.dzkd.soft.mapper.UserMapper;
import com.dzkd.soft.pojo.User;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.jcp.xml.dsig.internal.DigesterOutputStream;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserService {

    @Resource
    private UserMapper userMapper;

    public User selectById(Integer id) {   // 业务逻辑层
        return userMapper.selectById(id);
    }

    public User selectByIdAndUsername(Integer id, String username) {
        return userMapper.selectByIdAndUsername(id, username);
    }

    public User selectByUser(User user) {
        return userMapper.selectByUser(user);
    }

    public List<User> selectByPassword(String password) {
        return userMapper.selectByPassword(password);
    }

    public Map<String, Object> login(User user) {
        String password = user.getPassword();
        byte[] bytes = password.getBytes(StandardCharsets.UTF_8);
        String passwordMD5 = DigestUtils.md5DigestAsHex(bytes);
        user.setPassword(passwordMD5);

        User loginUser = userMapper.login(user);
        Map<String, Object> map = new HashMap<>();   // 初始化一个返回数据对象
        if(loginUser == null) {   // 代表用户名不存在
            map.put("code", 50001);
            map.put("msg", "用户名或者密码错误");
        }
        else {                    // 代表用户登录成功 用户名和密码正确
            loginUser.setLastLoginTime(new Timestamp(System.currentTimeMillis()));
            userMapper.updateLastLoginTime(loginUser);   // 更新最后的登录时间

            map.put("code", 200);
            map.put("msg", "登录成功");
            map.put("user", loginUser);
        }
        return map;
    }

    public Map<String, Object> selectByPage(int page, int limit, String username, String age) {
        // page 页码   (page-1)*limit=起始下标index
        int index = (page - 1) * limit;   // 计算出起始下标
        List<User> userList = userMapper.selectByPage(index, limit, username, age);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "查询成功");
        map.put("count", userMapper.selectCount(username, age));
        map.put("data", userList);
        return map;
    }

    public Map<String, Object> selectByPage_Black(int page, int limit, String username, String age) {
        // page 页码   (page-1)*limit=起始下标index
        int index = (page - 1) * limit;   // 计算出起始下标
        List<User> userList = userMapper.selectByPage_Black(index, limit, username, age);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "查询成功");
        map.put("count", userMapper.selectCount_Black(username, age));
        map.put("data", userList);
        return map;
    }

    public Map<String, Object> insert(User user) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis()); // 写入一个注册时间 new Date()代表当前服务器系统时间
        user.setRegTime(timestamp);

        String password = user.getPassword();
        byte[] bytes = password.getBytes(StandardCharsets.UTF_8);
        String passwordMD5 = DigestUtils.md5DigestAsHex(bytes);
        user.setPassword(passwordMD5);

        userMapper.insert(user);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "新增用户成功");
        return map;
    }

    public Map<String, Object> insert_Black(User user) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis()); // 写入一个注册时间 new Date()代表当前服务器系统时间
        user.setRegTime(timestamp);

        String password = user.getPassword();
        byte[] bytes = password.getBytes(StandardCharsets.UTF_8);
        String passwordMD5 = DigestUtils.md5DigestAsHex(bytes);
        user.setPassword(passwordMD5);

        userMapper.insert_Black(user);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "新增用户成功");
        return map;
    }

    public Map<String, Object> deleteById(Integer id) {
        userMapper.deleteById(id);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "删除用户成功");
        return map;
    }

    public Map<String, Object> deleteById_Black(Integer id) {
        userMapper.deleteById_Black(id);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "删除用户成功");
        return map;
    }

    public Map<String, Object> updateAge(Integer id, Integer age) {
        userMapper.updateAge(id, age);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "更新用户成功");
        return map;
    }
}
