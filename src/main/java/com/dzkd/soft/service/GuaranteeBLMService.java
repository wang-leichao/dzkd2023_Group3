package com.dzkd.soft.service;

import com.dzkd.soft.mapper.GuaranteeBLMMapper;
import com.dzkd.soft.pojo.GuaranteeBLM;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class GuaranteeBLMService  {
    @Resource
    private GuaranteeBLMMapper guaranteeBLMMapper;

    public Map<String, Object> selectByPage(int page,
                                            int limit,
                                            String bLName,
                                            String bLRuleNumb,
                                            String riskNumb,
                                            String affectServerNumb,
                                            String bLClassification,
                                            String state,
                                            Timestamp lastExecuteTime,
                                            String operation) {
        // page 页码   (page-1)*limit=起始下标index
        int index = (page - 1) * limit;   // 计算出起始下标
        List<GuaranteeBLMMapper> guaranteeBLMList = guaranteeBLMMapper.selectByPage(index,
                                                                                    limit,
                                                                                    bLName,
                                                                                    bLRuleNumb,
                                                                                    riskNumb,
                                                                                    affectServerNumb,
                                                                                    bLClassification,
                                                                                    state,
                                                                                    lastExecuteTime,
                                                                                    operation);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "查询成功");
        map.put("count", guaranteeBLMMapper.selectCount(bLName,
                                                        bLRuleNumb,
                                                        riskNumb,
                                                        affectServerNumb,
                                                        bLClassification,
                                                        state,
                                                        lastExecuteTime,
                                                        operation));
        map.put("data", guaranteeBLMList);
        return map;
    }
    public Map<String, Object> insert(GuaranteeBLM guaranteeBLM) {

        guaranteeBLMMapper.insert(guaranteeBLM);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "新增基线成功");
        return map;
    }
    public Map<String, Object> deleteById(Integer id) {
        guaranteeBLMMapper.deleteById(id);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "删除基线成功");
        return map;
    }
    public Map<String, Object> update(Integer id,
                                      String riskNumb,
                                      String affectServerNumb,
                                      String state,
                                      Timestamp lastExecuteTime,
                                      String operation) {
        guaranteeBLMMapper.update(id,riskNumb,affectServerNumb,state,lastExecuteTime,operation);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "更新基线成功");
        return map;
    }
}
