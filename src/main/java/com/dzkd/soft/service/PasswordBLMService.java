package com.dzkd.soft.service;

import com.dzkd.soft.mapper.PasswordBLMMapper;
import com.dzkd.soft.pojo.PasswordBLM;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PasswordBLMService {
    @Resource
    private PasswordBLMMapper PasswordBLMMapper;
    public PasswordBLM selectById(Integer id)
    {
        return PasswordBLMMapper.selectById(id);
    }

    public Map<String, Object> selectByPage(int page, int limit, String name, String category, String description, String level) {
        // page 页码   (page-1)*limit=起始下标index
        int index = (page - 1) * limit;   // 计算出起始下标
        List<PasswordBLM> PasswordBLMList = PasswordBLMMapper.selectByPage(index, limit, name, category, description, level);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "查询成功");
        map.put("count", PasswordBLMMapper.selectCount(name, category));
        map.put("data", PasswordBLMList);
        return map;
    }

    public Map<String, Object> insert(PasswordBLM passwordBLM){
        PasswordBLMMapper.insert(passwordBLM);
        Map<String,Object> map=new HashMap<>();
        map.put("code", 200);
        map.put("msg", "新增规则成功");
        return map;
    }

    public Map<String, Object> deleteById(Integer id) {
        PasswordBLMMapper.deleteById(id);
        Map<String,Object> map=new HashMap<>();
        map.put("code", 200);
        map.put("msg", "删除规则成功");
        return map;
    }

    public Map<String, Object> updateLevel(Integer id,String level) {
        PasswordBLMMapper.updateLevel(id,level);
        Map<String,Object> map=new HashMap<>();
        map.put("code", 200);
        map.put("msg", "更新规则成功");
        return map;
    }
}
