package com.dzkd.soft.service;

import com.dzkd.soft.mapper.MonitorMapper;
import com.dzkd.soft.pojo.Monitor;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class monitorService {

    @Resource
    private MonitorMapper monitorMapper;

    public Monitor selectById(Integer id) {   // 业务逻辑层
        return monitorMapper.selectById(id);
    }

    public Monitor selectByIdAndPortnum(Integer id, String portnum) {
        return monitorMapper.selectByIdAndPortnum(id, portnum);
    }

    public Monitor selectByMonitor(Monitor monitor) {
        return monitorMapper.selectByMonitor(monitor);
    }

    public List<Monitor> selectByPortagreement(String portagreement) {
        return monitorMapper.selectByPortagreement(portagreement);
    }


    public Map<String, Object> selectByPage(int page, int limit, String portnum, String processname) {
        // page 页码   (page-1)*limit=起始下标index
        int index = (page - 1) * limit;   // 计算出起始下标
        List<Monitor> monitorList = monitorMapper.selectByPage(index, limit, portnum, processname);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "查询成功");
        map.put("count", monitorMapper.selectCount(portnum, processname));
        map.put("data", monitorList);
        return map;
    }

    public Map<String, Object> insert(Monitor monitor) {


        monitorMapper.insert(monitor);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "新增用户成功");
        return map;
    }

    public Map<String, Object> deleteById(Integer id) {
        monitorMapper.deleteById(id);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "删除用户成功");
        return map;
    }

}
