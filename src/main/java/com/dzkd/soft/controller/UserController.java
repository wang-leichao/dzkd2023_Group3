package com.dzkd.soft.controller;

import com.dzkd.soft.pojo.User;
import com.dzkd.soft.service.UserService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    @Resource
    private UserService userService;

    // 所有这些命名按照企业规范，必须一致，但是在java语法本身是没有此约束的
    @RequestMapping("/selectById")
    public User selectById(@RequestParam Integer id) {
        return userService.selectById(id);
    }

    @RequestMapping("/selectBIAU")
    public User selectByIdAndUsername(@RequestParam Integer id,
                                      @RequestParam String username) {
        return userService.selectByIdAndUsername(id, username);
    }

    @RequestMapping("/selectByUser")
    public Map<String, Object> selectByUser(@RequestBody User user) {
        User response_user = userService.selectByUser(user);
        Map<String, Object> map = new HashMap<>();   // 初始化一个返回数据对象
        map.put("code", 200);
        map.put("user", response_user);                 //
        return map;
    }

    @RequestMapping("/selectByPassword")
    public Map<String, Object> selectByPassword(@RequestParam String password) {
        // List返回到浏览器是一个JSONArray
        List<User> userList = userService.selectByPassword(password);
        Map<String, Object> map = new HashMap<>();   // 初始化一个返回数据对象
        map.put("code", 200);
        map.put("list", userList);
        return map;
    }

    @RequestMapping("/user/login")
    public Map<String, Object> login(@RequestBody User user) {
        return userService.login(user);
    }

    @RequestMapping("/selectByPage")
    public Map<String, Object> selectByPage(@RequestParam int page,
                                            @RequestParam int limit,
                                            String username,
                                            String age) {
        return userService.selectByPage(page, limit, username, age);
    }

    @RequestMapping("/selectByPage_Black")
    public Map<String, Object> selectByPage_Black(@RequestParam int page,
                                                  @RequestParam int limit,
                                                  String username,
                                                  String age) {
        return userService.selectByPage_Black(page, limit, username, age);
    }

    @RequestMapping("/user/insert")
    public Map<String, Object> insert(@RequestBody User user) {
        return userService.insert(user);
    }

    @RequestMapping("/user/insert_Black")
    public Map<String, Object> insert_Black(@RequestBody User user) {
        return userService.insert_Black(user);
    }

    @RequestMapping("/user/deleteById")
    public Map<String, Object> deleteById(@RequestParam Integer id) {
        return userService.deleteById(id);
    }

    @RequestMapping("/user/deleteById_Black")
    public Map<String, Object> deleteById_Black(@RequestParam Integer id) {
        return userService.deleteById_Black(id);
    }

    @RequestMapping("/user/updateAge")
    public Map<String, Object> updateAge(@RequestParam Integer id, @RequestParam Integer age) {
        return userService.updateAge(id, age);
    }
}
