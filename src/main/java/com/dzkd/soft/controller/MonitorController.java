package com.dzkd.soft.controller;

import com.dzkd.soft.pojo.Monitor;
import com.dzkd.soft.service.monitorService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class MonitorController {

    @Resource
    private monitorService monitorService;

    // 所有这些命名按照企业规范，必须一致，但是在java语法本身是没有此约束的
    @RequestMapping("/selectByMonitor")
    public Map<String, Object> selectByMonitor(@RequestBody Monitor monitor) {
        Monitor response_monitor = monitorService.selectByMonitor(monitor);
        Map<String, Object> map = new HashMap<>();   // 初始化一个返回数据对象
        map.put("code", 200);
        map.put("monitor", response_monitor);                 //
        return map;
    }

    @RequestMapping("/selectByPortagreement")
    public Map<String, Object> selectByPortagreement(@RequestParam String portagreement) {
        // List返回到浏览器是一个JSONArray
        List<Monitor> monitorList = monitorService.selectByPortagreement(portagreement);
        Map<String, Object> map = new HashMap<>();   // 初始化一个返回数据对象
        map.put("code", 200);
        map.put("list", monitorList);
        return map;
    }

//    @RequestMapping("/monitor/login")
//    public Map<String, Object> login(@RequestBody Monitor monitor) {
//        return monitorService.login(monitor);
//    }

    @RequestMapping("/monitor/selectByPage")
    public Map<String, Object> selectByPage(@RequestParam int page,
                                            @RequestParam int limit,
                                            String monitorname,
                                            String processname) {
        return monitorService.selectByPage(page, limit, monitorname, processname);
    }

    @RequestMapping("/monitor/insert")
    public Map<String, Object> insert(@RequestBody Monitor monitor) {
        return monitorService.insert(monitor);
    }

    @RequestMapping("/monitor/deleteById")
    public Map<String, Object> deleteById(@RequestParam Integer id) {
        return monitorService.deleteById(id);
    }
}
