package com.dzkd.soft.controller;

import com.dzkd.soft.pojo.PasswordBLM;

import com.dzkd.soft.service.PasswordBLMService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@RestController
public class PasswordBLMController {
    @Resource
    private PasswordBLMService PasswordBLMService;

    @RequestMapping("/pwd/selectById")
    public PasswordBLM selectById(@RequestParam Integer id){
        return PasswordBLMService.selectById(id);
    }

    @RequestMapping("/pwd/selectByPage")
    public Map<String, Object> selectByPage(@RequestParam int page,
                                            @RequestParam int limit,
                                            String name,
                                            String category,
                                            String description,
                                            String level) {
        return PasswordBLMService.selectByPage(page, limit, name, category, description, level);
    }

    @RequestMapping("/pwd/insert")
    public Map<String, Object> insert(@RequestBody PasswordBLM passwordBLM) {
        return PasswordBLMService.insert(passwordBLM);
    }

    @RequestMapping("/pwd/deleteById")
    public Map<String, Object> deleteById(@RequestParam Integer id) {
        return PasswordBLMService.deleteById(id);
    }

    @RequestMapping("/pwd/updateLevel")
    public Map<String, Object> updateAge(@RequestParam Integer id,
                                         @RequestParam String level) {
        return PasswordBLMService.updateLevel(id,level);
    }
}
