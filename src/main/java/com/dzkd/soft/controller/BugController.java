package com.dzkd.soft.controller;

import com.dzkd.soft.pojo.Bug;
import com.dzkd.soft.service.BugService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class BugController {

    @Resource
    private BugService bugService;

    // 所有这些命名按照企业规范，必须一致，但是在java语法本身是没有此约束的
    @RequestMapping("/bug/selectById")
    public Bug selectById(@RequestParam Integer id) {
        return bugService.selectById(id);
    }



    @RequestMapping("/selectBybug")
    public Map<String, Object> selectBybug(@RequestBody Bug bug) {
        Bug response_bug = bugService.selectByBug(bug);
        Map<String, Object> map = new HashMap<>();   // 初始化一个返回数据对象
        map.put("code", 200);
        map.put("bug", response_bug);                 //
        return map;
    }

    @RequestMapping("/selectBybuglevel")
    public Map<String, Object> selectBybuglevel(@RequestParam String buglevel) {
        // List返回到浏览器是一个JSONArray
        List<Bug> bugList = bugService.selectBybuglevel(buglevel);
        Map<String, Object> map = new HashMap<>();   // 初始化一个返回数据对象
        map.put("code", 200);
        map.put("list", bugList);
        return map;
    }

//    @RequestMapping("/bug/login")
//    public Map<String, Object> login(@RequestBody Bug bug) {
//        return bugService.login(bug);
//    }

    @RequestMapping("/bug/selectByPage")
    public Map<String, Object> selectByPage(@RequestParam int page,
                                            @RequestParam int limit,
                                            String bugname,
                                            String buglevel) {
        return bugService.selectByPage(page, limit, bugname, buglevel);
    }

    @RequestMapping("/bug/insert")
    public Map<String, Object> insert(@RequestBody Bug bug) {
        return bugService.insert(bug);
    }

    @RequestMapping("/bug/deleteById")
    public Map<String, Object> deleteById(@RequestParam Integer id) {
        return bugService.deleteById(id);
    }

    @RequestMapping("/bug/updatenum")
    public Map<String, Object> updatenum(@RequestParam Integer id, @RequestParam Integer num) {
        return bugService.updatenum(id, num);
    }
}
