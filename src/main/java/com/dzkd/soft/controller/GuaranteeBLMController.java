package com.dzkd.soft.controller;

import com.dzkd.soft.pojo.GuaranteeBLM;

import com.dzkd.soft.service.GuaranteeBLMService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Map;

@RestController
public class GuaranteeBLMController {
    @Resource
    private GuaranteeBLMService guaranteeBLMService;

    @RequestMapping("/gBLM/selectByPage")
    public Map<String, Object> selectByPage(@RequestParam int page,
                                            @RequestParam int limit,
                                            String bLName,
                                            String bLRuleNumb,
                                            String riskNumb,
                                            String affectServerNumb,
                                            String bLClassification,
                                            String state,
                                            Timestamp lastExecuteTime,
                                            String operation) {
        return guaranteeBLMService.selectByPage(page,
                                                limit,
                                                bLName,
                                                bLRuleNumb,
                                                riskNumb,
                                                affectServerNumb,
                                                bLClassification,
                                                state,
                                                lastExecuteTime,
                                                operation);
    }

    @RequestMapping("/gBLM/insert")
    public Map<String, Object> insert(@RequestBody GuaranteeBLM guaranteeBLM) {
        return guaranteeBLMService.insert(guaranteeBLM);
    }
    @RequestMapping("/gBLM/deleteById")
    public Map<String, Object> deleteById(@RequestParam Integer id) {
        return guaranteeBLMService.deleteById(id);
    }
    @RequestMapping("/gBLM/update")
    public Map<String, Object> update(@RequestParam Integer id,
                                      @RequestParam String riskNumb,
                                      @RequestParam String affectServerNumb,
                                      @RequestParam String state,
                                      @RequestParam Timestamp lastExecuteTime,
                                      @RequestParam String operation) {
        return guaranteeBLMService.update(id, riskNumb, affectServerNumb, state, lastExecuteTime, operation);
    }
}
