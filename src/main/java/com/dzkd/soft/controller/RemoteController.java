package com.dzkd.soft.controller;


import com.dzkd.soft.service.RemoteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@RestController
public class RemoteController {
    @Resource
    private RemoteService remoteService;



    @RequestMapping("/remoter/selectByPage")
    public Map<String, Object> selectByPage(@RequestParam int page,
                                            @RequestParam int limit,
                                            String deal_status,
                                            String warn_type,
                                            String host_ip) {
        return remoteService.selectByPage(page, limit, deal_status, warn_type,host_ip);
    }

    @RequestMapping("/remoter/updateDealStatus")
    public Map<String, Object> updateDealStatus(@RequestParam Integer id, @RequestParam String deal_status) {
        return remoteService.updateDealStatus(id, deal_status);
    }


}
